<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>
      <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

</head>
	<body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid">
            <a class="navbar-brand" href="IndexServlet">肉グルメ</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">



            </div>
                    </div>
                </nav>

						<c:if test="${errMsg != null}" >
				    <div class="alert alert-danger" role="alert">
					  ${errMsg}
					</div>
				</c:if>

				<c:if test="${errmsg != null}" >
					    <div class="alert alert-danger" role="alert">
						  ${errmsg}
						</div>
					</c:if>

         <div class="container">
		  <div class="box">
			  <div class="row">
				  <div class="col-md-6 mt-4">
					  <div class="well">
						  <form id="loginForm" method="post" action="LoginServlet" novalidate>
							  <div class="form-group">
								  <label for="username" class="control-label">ユーザーネーム</label>
								  <input type="text" class="form-control" id="userName" name="userName"  title="Please enter you username" placeholder="example@gmail.com">
							  </div>
							  <div class="form-group">
								  <label for="password" class="control-label">パスワード</label>
								  <input type="password" class="form-control" id="password" name="password"  title="Please enter your password">
							  </div>
							 <br>
							  <button type="submit" class="btn btn-success btn-block">ログイン</button>

						  </form>
					  </div>
				  </div>
				  <div class="col-md-6 mt-5 pt-4">
                      <br><br>
					  <p class="lead">会員登録は <span class="text-success">無料</span>今すぐ登録！</p>
					  <p>まだ会員でない方は、以下より会員登録をお願いいたします。</p>
					  <p><a href="SignUpServlet" class="btn btn-info btn-block">新規登録はこちらから!</a></p>
				  </div>
			  </div>
		  </div>
    </div><!-- /container -->
	</body>
</html>
