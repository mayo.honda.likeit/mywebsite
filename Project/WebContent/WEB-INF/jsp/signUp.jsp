<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>新規登録画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
            <link rel="stylesheet" href="style.css" type="text/css"/>

            <link rel="stylesheet" href="css/style.css">

</head>
	<body>
		<!--        ナビゲーション -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
    <a class="navbar-brand" href="IndexServlet">肉グルメ</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">



            </div>
            </div>

        </nav>

        <c:if test="${errMsg != null}" >
				    <div class="alert alert-danger" role="alert">
					  ${errMsg}
					</div>
				</c:if>

				<c:if test="${errmsg != null}" >
					    <div class="alert alert-danger" role="alert">
						  ${errmsg}
						</div>
					</c:if>

        <br>
        <div class="container">
            <div class="ccc mt-3">
                <h5>新規登録</h5>
            </div>
       <br><br>
        <div class="torokubox mt-5 pt-5">
            <form action="#" method="post">
                <div class="ml-4">

                    <p>名前</p>
                    <input type="text" name="name" placeholder="名前を入力">
                    <p>ユーザーネーム</p>
                    <P>例）example@gmail.com</P>
                    <input type="email" name="email" placeholder="アドレスを入力">
                    <p>パスワード</p>
                    <input type="password" name="password" placeholder="パスワードを入力">
                    <p></p>
                    <input  class="btn btn-success ml-5" type="submit" name="submit" value="新規登録">
                </div>
            </form>
        </div>

        </div>

	</body>
</html>
