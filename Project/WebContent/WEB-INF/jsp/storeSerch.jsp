<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>検索結果画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

	<link rel="stylesheet" href="css/style.css">

        <link rel="stylesheet" href="css/index.css">
         <link rel="stylesheet" href="css/evaluation.css">
         <link rel="stylesheet" href="css/storeSearch.css">

    <style>
        .kensaku{
            text-align: center;
        }
		.yakiniku{
			widht: 200px;
			height: 200px;
		}
    </style>
</head>
	<body>
<!--        ナビゲーション -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
    <a class="navbar-brand" href="IndexServlet">肉グルメ</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="UserDataServlet?id=${user.id}">マイページ</a>
        </li>

           </ul>


            </div>
            </div>
            <ul class="nav justify-content-end">
                 <li class="dropdown">
                     <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>

                    </li>
            </ul>
        </nav>

        <div class="kensaku">



        <div class="container">
        <h2>検索結果</h2>
        </div>

        <div class="container">
        <p></p>
        </div>
        </div>



		 <div class="container mt-5">

			<c:forEach var="shop" items="${shopList}" >

			<section>
    <div class="card-box-wrap">
        <a href="ShopDetailsServlet?id=${shop.id}" class="card-box">
            <img src="img/${shop.fileName}" alt="card-1" class="yakiniku">
            <div class="card-text">
                <h2>${shop.name}</h2>
                <p>${shop.station}</p>
            </div>
        </a>

    </div>
</section>
 </c:forEach>
        </div>


            <!--  <div class="row">
                 <div class="col-sm-6">
                <div class="card mb-3 ml-5" style="max-width: 540px;">
                  <div class="row g-0">
                    <div class="col-md-4">
                    <a  href="ShopDetailsServlet?id=${shop.id}">
                      <img src="img/${shop.fileName}" alt="焼肉" class="yakiniku">
                      </a>
                    </div>




                     <div class="col-md-6 ml-1">
                    <div class="card">
                      <div class="card-body">
                        <h5 class="card-title">${shop.name}</h5>
                        <p class="card-text">${shop.station}</p>
                        <p class="card-text"><small class="text-muted">${shop.star}</small></p>
                      </div>
                    </div>
                    </div>

                  </div>

                </div>
                </div>
            </div>-->



	</body>
</html>
