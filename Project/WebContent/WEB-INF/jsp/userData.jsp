<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>会員情報画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

            <link rel="stylesheet" href="css/userData.css">
</head>
	<body>
<!--        ナビゲーション-->
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
    <a class="navbar-brand" href="IndexServlet">肉グルメ</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">

           </ul>

    </div>
            </div>
            <ul class="nav justify-content-end">
                 <li class="dropdown">
                     <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>

                    </li>
            </ul>
        </nav>

        <div class="container">
            <div class="ccc mt-3">
            <h4>会員情報</h4>
            </div>
            <div class="row">
                <div class="col-6">
                <div class="card-group mt-4">
                    <div class="card">
                  <div class="card-body">
                    <label for="name" class="control-label">名前</label>
                   <p>${user.name}</p>
                      <label for="mail" class="control-label">ユーザーネーム(アドレス)</label>
                      <p>${user.userName}</p>
                  </div>
                    </div>
                </div>
                </div>

                <div class="col-4">
                    <div class="mb-5">
                        <h4>予約情報</h4>
                            <table class="table table-borderless">
                              <tr>
                              		<th>店舗ID</th>
                                  	<th>日時</th>
                            　   　	<th>時間</th>


                            　</tr>
                            <c:forEach var="reservation" items="${reservationList}">
                                <tr>
                                	 <td><a href="ShopDetailsServlet?id=${reservation.shopId }">${reservation.id }</a> </td>
                                     <td>${reservation.formatDate }</td>
                            　       <td>${reservation.formatData1 }</td>


                                </tr>
                             </c:forEach>
                            </table>
                    </div>

                </div>
            </div>

<!--            閲覧履歴-->
            <h3 class="mt-4" >閲覧履歴</h3>

            <table class="table table-striped table-hover">
                <tr>
                  <th></th>
                  <th>店名</th>
                  <th>評価数</th>
                </tr>

			<c:forEach var="shop" items="${history}" >
                <tr>
                    <td><a href="ShopDetailsServlet?id=${shop.id}" class="btn-flat-bottom-border">
                          <span>詳細</span></a>
                    </td>
                    <td>${shop.name}</td>
                    <td>
                        <p><span class="star5_rating" data-rate="${shop.star}"></span></p></td>
                </tr>
             </c:forEach>

            </table>



		</div>

	</body>
</html>
