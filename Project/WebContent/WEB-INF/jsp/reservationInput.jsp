<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>予約入力画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

     <link rel="stylesheet" href="css/style.css">

</head>
	<body>
        <!--        ナビゲーション -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
    <a class="navbar-brand" href="IndexServlet">肉グルメ</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="UserDataServlet?id=${user.id}">マイページ</a>
        </li>

           </ul>


            </div>
            </div>
            <ul class="nav justify-content-end">
                 <li class="dropdown">
                     <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>

                    </li>
            </ul>
        </nav>

        <div class="container">
		  <h3 class="mt-5" style="text-align: center">予約入力</h3>
            <p class="lead-form">ご予約はこちらから！</p>

            <form action="ReservationInputServlet" method="post">

            <input type="hidden" name="shop_id" value=${shopid } size="15"
			maxlength="20">

                <div class="form-group">
                  <label for="date">日付</label>
                   <input id="date" type="date" name="reservationDate">
                </div>

                <div class="form-group">
                <label for="time">時間</label>
                 <input id="time" type="time" name="reservationTime" list="data1" >
                    <datalist id="data1">
                    <option value="17:00"></option>
                    <option value="17:30"></option>
                    <option value="18:00"></option>
                    <option value="18:30"></option>
                    <option value="19:00"></option>
                    <option value="19:30"></option>
                    <option value="20:00"></option>
                    <option value="20:30"></option>
                    <option value="21:00"></option>
                    </datalist>
                </div>

					<input type="hidden" name="create_date" value=${reservation.createDate } size="15"
					maxlength="20">

                <div class="btn-area">

                    <input type="submit" value="予約する" class="btn btn-danger">

                    <input type="reset" value="リセット" class="btn btn-secondary">
                </div>
            </form>

        </div>
	</body>
</html>
