<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>トップページ</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

    	    <link rel="stylesheet" href="css/index.css">
    	    <link rel="stylesheet" href="css/userData.css">

</head>
	<body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
    <a class="navbar-brand" href="IndexServlet">肉グルメ</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="UserDataServlet?id=${user.id}">マイページ</a>
        </li>

           </ul>

            <ul class="navbar-nav ">
                 <li class="dropdown">
                 <c:if test="${user.id == null}">
                      <a href="LoginServlet" class="navbar-link logout-link">無料会員登録/ログイン</a>
                      </c:if>
                    </li>
            </ul>
    </div>
            </div>
            <ul class="nav justify-content-end">
                 <li class="dropdown">
                     <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>

                    </li>
            </ul>
        </nav>

        <div class="img-main">
            <div class="img-main-box">
                <img src="img/bnr-main.jpg" alt="焼肉" >
                <h2 class="mt-5 pt-5">
                    <span class="title">焼肉</span><br>
                    <span class="en-title">YAKINIKU</span>
                </h2>
            </div>
        </div>

        <div class="card-group mt-4">
            <div class="card">
		      <div class="card-body">
        <form action="IndexServlet" method="post">
        <div class="form-group">



            <label for="money">予算(何円から)</label>
            <input type="text" name="start_price" id="start_price" list="data2">
            <datalist id="data2">
            <option value="4000円"></option>
            <option value="5000円"></option>
            <option value="6000円"></option>
            <option value="7000円"></option>
            <option value="8000円"></option>
            <option value="9000円"></option>
            <option value="10000円"></option>
            <option value="11000円"></option>
            <option value="12000円"></option>
            <option value="13000円"></option>
            <option value="14000円"></option>
            <option value="15000円"></option>
            </datalist>

            <label for="money">予算(何円まで)</label>
            <input type="text" name="end_price" id="end_price" list="data3">
            <datalist id="data3">
            <option value="5000円"></option>
            <option value="6000円"></option>
            <option value="7000円"></option>
            <option value="8000円"></option>
            <option value="9000円"></option>
            <option value="10000円"></option>
            <option value="11000円"></option>
            <option value="12000円"></option>
            <option value="13000円"></option>
            <option value="14000円"></option>
            <option value="15000円"></option>
            <option value="16000円"></option>
            <option value="17000円"></option>
            <option value="18000円"></option>
            <option value="19000円"></option>
            <option value="20000円"></option>
            </datalist>

            <label for="text">最寄り駅</label>
            <input id="station" type="text" name="station" list="data4">
            <datalist id="data4">
            <option value="恵比寿駅(渋谷区)"></option>
            <option value="本郷三丁目駅(文京区)"></option>
            <option value="西新井大師西駅(足立区)"></option>
            <option value="渋谷駅(渋谷区)"></option>
            <option value="清澄白河駅(江東区)"></option>
            <option value="月島駅(中央区)"></option>
            <option value="銀座駅(中央区)"></option>
            <option value="鶯谷駅(台東区)"></option>
            <option value="町屋駅(荒川区)"></option>
            <option value="不動前駅(品川区)"></option>
            </datalist>

            <button type="submit" value="検索" class="btn btn-primary form-submit ml-4">検索</button>

          </div>

        </form>
                </div>
            </div>
        </div>
        <br><br>


        <div class="container">
            <div class="row">
        <div class="col span-10">
           <!--ランキング-->


	 <c:forEach var="rank" items="${rankList}" >
            <div class="contents">
            <ul class="ranking-box">
            <li class="no01">

            <br><br>
                <P>店名:　${rank.name}</P>
            <p class="item-img"><img src="img/${rank.fileName}" width="200" height="200" alt="焼肉屋" /></p>
            <p>評価数: ${rank.star}<p><span class="star5_rating" data-rate="${rank.star}"></span></p>
            <p class="link-next02"><a href="ShopDetailsServlet?id=${rank.id}">店舗詳細</a></p>
            </li>
            </ul>
        	</div>
		</c:forEach>

        </div>
        <div class="col span-2">
           <!--            閲覧履歴-->
            <h3 class="mt-4" >閲覧履歴</h3>

            <table class="table table-striped table-hover">
                <tr>
                  <th></th>
                  <th>店名  </th>
                  <th>評価数</th>
                </tr>

			<c:forEach var="shop" items="${history}" >
                <tr>
                    <td><a href="ShopDetailsServlet?id=${shop.id}" class="btn-flat-bottom-border">
                          <span>詳細</span></a>
                    </td>
                    <td>${shop.name}</td>
                    <td>
                        <p><span class="star5_rating" data-rate="${shop.star}"></span></p></td>
                </tr>
            </c:forEach>


            </table>
        </div>
        </div>
    </div>

	</body>
</html>
