<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>店舗詳細画面</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
        <link rel="stylesheet" href="style.css" type="text/css"/>

        <link rel="stylesheet" href="css/style.css">

        <link rel="stylesheet" href="css/index.css">

        <link rel="stylesheet" href="css/evaluation.css">

    <style>
            .syousai{
            text-align: center;
        }



    </style>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

</head>
	<body>
        <!--        ナビゲーション -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
    <a class="navbar-brand" href="IndexServlet">肉グルメ</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="UserDataServlet?id=${user.id}">マイページ</a>
        </li>

           </ul>


            </div>
            </div>
            <ul class="nav justify-content-end">
                 <li class="dropdown">
                     <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>

                    </li>
            </ul>
        </nav>
        <div class="container">
            <div class="syousai mt-5">
                <h3> 店舗詳細</h3>
            </div>
            <div  style="text-align: right;">
                <a href="ReservationInputServlet?id=${shop.id}" class="link-info">予約はこちらから</a>
            </div>
            <div  style="text-align: right;">
                <a href="EvaluationServlet?id=${shop.id }" class="link-info">評価はこちらから</a>
            </div>
            <br><br><br>
            <p>店舗名:${shop.name}</p>
            <ul>
                <li>評価数: ${shop.star}   <span class="star5_rating" data-rate="${shop.star}"></span></li>
                <li>最寄り駅: ${shop.station}</li>
            </ul>

            <h5>口コミ</h5>
            <c:forEach var="evaluation" items="${evaluationList}" >
	            <ul>
	                <li>${user.name}　:  ${evaluation.comment } </li>
	            </ul>
			</c:forEach>
            <!-- 写真 <img src="img/${shop.fileName}" alt="焼肉" align="top">-->
            <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
  <ol class="carousel-indicators">
    <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"></li>
    <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"></li>
    <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="img/${shop.fileName}" class="d-block w-100" alt="焼肉">
    </div>
    <div class="carousel-item">
      <img src="img/${shop.fileName2}" class="d-block w-100" alt="焼肉2">
    </div>
    <div class="carousel-item">
      <img src="img/${shop.fileName3}" class="d-block w-100" alt="焼肉3">
    </div>
  </div>

  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </a>

</div>
            <br><br><br>
            <div style="text-align: center">
                <p>店舗詳細情報</p>
            </div>
            <table>
                  <tr>
                    <th>店舗名</th>
                    <td>${shop.name}</td>
                  </tr>
                  <tr>
                    <th>電話番号</th>
                    <td>${shop.phoneNumber}</td>
                  </tr>
                  <tr>
                    <th>住所</th>
                    <td>${shop.address}</td>
                  </tr>
                  <tr>
                    <th>交通手段</th>
                    <td>${shop.transportation}</td>
                  </tr>
                <tr>
                    <th>予算</th>
                    <td>${shop.startPrice}円から${shop.endPrice}円</td>
                  </tr>
                <tr>
                    <th>営業時間</th>
                    <td>${shop.formatData}～${shop.formatData1}</td>
                  </tr>
                <tr>
                    <th>席数</th>
                    <td>${shop.seatsNumber}</td>
                  </tr>
                <tr>
                    <th>個室</th>
                    <td>${shop.privateRoom}</td>
                  </tr>
            </table>
        </div>

	</body>
</html>
