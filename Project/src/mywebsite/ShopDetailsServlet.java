package mywebsite;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Evaluation;
import beans.Shop;
import beans.User;
import dao.EvaluationDAO;
import dao.ShopDAO;

/**
 * Servlet implementation class ShopDetailsServlet
 */
@WebServlet("/ShopDetailsServlet")//店舗詳細
public class ShopDetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShopDetailsServlet() {


    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// idを引数にして、idに紐づくユーザ情報を出力する

		ShopDAO shopDao = new ShopDAO();
		Shop shop = shopDao.syousai(id);



		User user = (User) session.getAttribute("user");



		//口コミ一覧の情報を取得
		EvaluationDAO evaluationdao = new EvaluationDAO();
		List<Evaluation> evaluationList = evaluationdao.kutikomi(id);



		//historyを取得
		ArrayList<Shop> history = (ArrayList<Shop>) session.getAttribute("history");


		//セッションにヒストリーがない場合ヒストリーを作成
		if (history == null) {
			history = new ArrayList<Shop>();
			//履歴に追加。
			history.add(shop);

		}else {
			///ヒストリーに入っているものと同じ名前は追加しない
			//ヒストリーをfor文
			 for (Shop shop1 : history) {

				 if(!(shop.getName().equals(shop1.getName())) ) {
					//履歴に追加。
						history.add(shop);
						break;

				 }
				 else if(shop.getName().equals(shop1.getName())) {
					 break;
				 }


			}
		}





			//historyの情報更新
			session.setAttribute("history", history);





		// 店舗情報をリクエストスコープにセットしてjspにフォワード

		request.setAttribute("user", user);
		request.setAttribute("evaluationList", evaluationList);
		request.setAttribute("shop", shop);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shopDetails.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
