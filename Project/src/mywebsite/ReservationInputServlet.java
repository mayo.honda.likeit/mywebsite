package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.ReservationDAO;

/**
 * Servlet implementation class ReservationInputServlet
 */
@WebServlet("/ReservationInputServlet")//予約入力
public class ReservationInputServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReservationInputServlet() {

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String shopid = request.getParameter("id");

		request.setAttribute("shopid", shopid);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reservationInput.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");



		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");




		// リクエストパラメータの入力項目を取得

		String shopid = request.getParameter("shop_id");
		String reservationDate = request.getParameter("reservationDate");
		String reservationTime = request.getParameter("reservationTime");


		//daoを実行
		ReservationDAO reservationdao = new ReservationDAO();
		reservationdao.yoyaku(user.getId(),shopid,reservationDate,reservationTime);

		//登録成功時の処理
		// 登録完了のサーブレットにリダイレクト
		response.sendRedirect("ReservationDoneServlet");




	}

}
