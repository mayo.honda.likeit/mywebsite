package mywebsite;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Shop;
import beans.User;
import dao.ShopDAO;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet("/IndexServlet")//トップページ
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;



	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログイン情報がない場合はログイン画面に
				//ある場合はその画面に

				HttpSession session = request.getSession();
				User user = (User) session.getAttribute("user");
				if (user == null) {
					// フォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
					dispatcher.forward(request, response);

				} else {

					//閲覧履歴
					//historyを取得
					ArrayList<Shop> history = (ArrayList<Shop>) session.getAttribute("history");




					session.setAttribute("history", history);

					//ランキング
					ShopDAO shopDao = new ShopDAO();

					List<Shop> rankList = shopDao.rank();

					request.setAttribute("rankList", rankList);

					//店舗id
					String shopid = request.getParameter("id");

					request.setAttribute("shopid", shopid);

					//トップページのjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
					dispatcher.forward(request, response);

				}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//検索情報を取得
				ShopDAO shopdao = new ShopDAO();
				String startPrice = request.getParameter("start_price");
				String endPrice = request.getParameter("end_price");
				String stationData = request.getParameter("station");

				List<Shop> shopList = shopdao.findbySearch(startPrice,endPrice,stationData);

				// リクエストスコープに検索情報をセット
				request.setAttribute("shopList", shopList);

				// 店舗検索のjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/storeSerch.jsp");
				dispatcher.forward(request, response);


	}

}
