package mywebsite;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Reservation;
import beans.Shop;
import beans.User;
import dao.ReservationDAO;
import dao.UserDAO;

/**
 * Servlet implementation class UserData
 */
@WebServlet("/UserDataServlet")//ユーザー情報
public class UserDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		Shop shop  = (Shop) session.getAttribute("shop");


		// URLからGETパラメータとしてIDを受け取る
					String id = request.getParameter("id");

					// 確認用：idをコンソールに出力
					System.out.println(id);

					// idを引数にして、idに紐づくユーザ情報を出力する

					UserDAO userDao = new UserDAO();
					User user1 = userDao.reference(id);



					//予約一覧の情報を取得
					ReservationDAO reservationdao = new ReservationDAO();
					List<Reservation> reservationList = reservationdao.yoyaku(id);





					//閲覧履歴
					//historyを取得

					ArrayList<Shop> history = (ArrayList<Shop>) session.getAttribute("history");
					session.setAttribute("history", history);



					// ユーザ情報をリクエストスコープにセットしてjspにフォワード
					//リクエストスコープにユーザー一覧情報をセット

					request.setAttribute("reservationList", reservationList);
					request.setAttribute("user", user1);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userData.jsp");
					dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



	}

}
