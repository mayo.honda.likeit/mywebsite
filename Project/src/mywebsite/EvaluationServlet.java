package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Shop;
import beans.User;
import dao.EvaluationDAO;

/**
 * Servlet implementation class EvaluationServlet
 */
@WebServlet("/EvaluationServlet")
public class EvaluationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EvaluationServlet() {

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Shop shop = (Shop) session.getAttribute("shop");

		session.setAttribute("shop", shop);
		String shopid = request.getParameter("id");

		//確認
		System.out.println(shopid);

		request.setAttribute("shopid", shopid);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/evaluation.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");

		// リクエストパラメータの文字コードを指定
				request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得


		String shopid = request.getParameter("shop_id");
		int star = Integer.parseInt(request.getParameter("star"));
		String comment = request.getParameter("comment");

		//daoを実行
				EvaluationDAO evaluationdao = new EvaluationDAO();
				evaluationdao.hyouka(user.getId(),shopid, star, comment);



				//登録成功時の処理
				// 登録完了のサーブレットにリダイレクト
				response.sendRedirect("IndexServlet");






	}

}
