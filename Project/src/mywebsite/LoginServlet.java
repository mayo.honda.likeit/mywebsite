package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")//ログイン
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ログインセッションがある場合、トップ画面にリダイレクトさせる

				HttpSession session = request.getSession();
				User user = (User) session.getAttribute("user");
				if (user == null) {
					// フォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
					dispatcher.forward(request, response);
					return;
				} else {
					// ユーザ一覧のサーブレットにリダイレクト
					response.sendRedirect("IndexServlet");
					return;
				}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得

		String username = request.getParameter("userName");
		String password = request.getParameter("password");


		//daoのメソッド実行
		UserDAO userDao = new UserDAO();
		User user = userDao.findByLoginInfo(username, password);




		//ログインIDが存在しない場合
		//ログインIDとパスワードが異なる場合
				if (user == null) {
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");
					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
					dispatcher.forward(request, response);
					return;
				}


				//暗号化
				UserDAO userDao1 = new UserDAO();
				userDao1.seacret(password);

				// テーブルに該当のデータが見つかった場合(ログイン成功時)
				// セッションにユーザの情報をセット
				HttpSession session = request.getSession();
				session.setAttribute("user", user);
				// トップページのサーブレットにリダイレクト
				response.sendRedirect("IndexServlet");



}
}
