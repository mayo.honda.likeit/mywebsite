package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class SignUpServlet
 */
@WebServlet("/SignUpServlet")//新規登録
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//フォーワード 新規登録画面へ
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signUp.jsp");
					dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

				// リクエストパラメータの文字コードを指定
				request.setCharacterEncoding("UTF-8");


				// リクエストパラメータの入力項目を取得
				String name = request.getParameter("name");
				String username = request.getParameter("email");
				String password = request.getParameter("password");

				String str = "";

				//入力項目に1つでも未入力がある場合
				if(name.equals(str) || username.equals(str) || password.equals(str)) {
					// リクエストスコープにエラーメッセージ
					request.setAttribute("errMsg", "入力された内容は正しくありません。");
					// 新規登録jspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signUp.jsp");
					dispatcher.forward(request, response);
					return;

				}


				//既に登録されているログインIDが入力された場合

				//daoのメソッド実行
				UserDAO userDao = new UserDAO();
				User user = userDao.findByNameandPassowrd(username, password);

				if (!(user == null)) {
					// リクエストスコープにエラーメッセージ
					request.setAttribute("errMsg", "入力された内容は正しくありません。");
					// 新規登録jspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signUp.jsp");
					dispatcher.forward(request, response);
					return;
				}

				//daoを実行
				UserDAO userDao1 = new UserDAO();
				userDao1.entry(name,username , password);

				//暗号化
				UserDAO userDao2 = new UserDAO();
				userDao2.seacret(password);





				//登録成功時の処理
				// トップページのサーブレットにリダイレクト
				response.sendRedirect("IndexServlet");


	}

}
