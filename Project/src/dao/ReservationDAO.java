package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import Base.DBManager;
import beans.Reservation;

public class ReservationDAO {

	//予約登録機能
	public void yoyaku(int userid,String shopid,String reservationDate,String reservationTime) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "INSERT INTO reservation(user_id,shop_id,reservation_date,reservation_time,create_date) VALUES (?,?,?,?,now())";

			stmt = con.prepareStatement(sql);


			stmt.setInt(1,userid);
			stmt.setString(2,shopid);
			stmt.setString(3,reservationDate);
			stmt.setString(4,reservationTime);


			stmt.executeUpdate();

			// 予約登録成功時の処理
			System.out.println("予約登録に成功しました");

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//ユーザーIDに紐づく全ての予約詳細
	public List<Reservation> yoyaku(String id) {
		Connection con = null;
		List<Reservation> reservationList = new ArrayList<Reservation>();
		try {
			// データベースへ接続
			con = DBManager.getConnection();


			// 確認済みのSQL
			String sql = "SELECT * FROM reservation WHERE user_id = ?";

			// SELECTを実行し、結果表を取得




			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();



			// 成功時の処理
			while (rs.next()) {
			int Id = rs.getInt("id");
			int userId = rs.getInt("user_id");
			int shopId = rs.getInt("shop_id");
			Date reservationDate = rs.getDate("reservation_date");
			Time reservationTime = rs.getTime("reservation_time");
			Date createDate = rs.getDate("create_date");

			Reservation reservation = new Reservation(Id,userId,shopId,reservationDate,reservationTime,createDate);

			System.out.println("予約詳細");

			reservationList.add(reservation);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return reservationList;
	}




}
