package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Base.DBManager;
import beans.Evaluation;

public class EvaluationDAO {

	//評価登録機能
	public void hyouka(int userid,String shopid,int star,String comment) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "INSERT INTO evaluation(user_id,shop_id,star,comment) VALUES (?,?,?,?)";

			stmt = con.prepareStatement(sql);


			stmt.setInt(1,userid);
			stmt.setString(2,shopid);
			stmt.setInt(3,star);
			stmt.setString(4,comment);

			// 予約登録成功時の処理
						System.out.println("評価登録に成功しました");

			stmt.executeUpdate();



		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}


	//評価表示機能
	public List<Evaluation> kutikomi(String id) {
		Connection con = null;
		List<Evaluation> evaluationList = new ArrayList<Evaluation>();
		try {
			// データベースへ接続
			con = DBManager.getConnection();


			// 確認済みのSQL
			String sql = "SELECT * FROM evaluation WHERE shop_id = ?";

			// SELECTを実行し、結果表を取得




			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();



			// 成功時の処理
			while (rs.next()) {
			int Id = rs.getInt("id");
			int userId = rs.getInt("user_id");
			int shopId = rs.getInt("shop_id");
			int star = rs.getInt("star");
			String comment = rs.getString("comment");


			Evaluation evaluation = new Evaluation(Id,userId,shopId,star,comment);

			System.out.println("口コミ");

			evaluationList.add(evaluation);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return evaluationList;
	}

}
