package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import Base.DBManager;
import beans.User;

public class UserDAO {

	//ログイン処理
	public User findByLoginInfo(String userName, String password) {
		Connection con = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "SELECT * FROM user WHERE user_name = ? and password = ?";

			//暗号化
			String result = seacret(password);

			System.out.println(result);



			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, userName);
			pStmt.setString(2, result);
			ResultSet rs = pStmt.executeQuery();


			// ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理
			System.out.println("ログインに成功しました");

			int idData = rs.getInt("id");
			String usernameData = rs.getString("user_name");
			String passData = rs.getString("password");
			return new User(idData,usernameData, passData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//新規登録

	public void entry(String name, String userName, String password) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "INSERT INTO user(name,user_name,password) VALUES (?,?,?)";

			//暗号化
			String result = seacret(password);

			System.out.println(result);



			stmt = con.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, userName);
			stmt.setString(3, result);

			stmt.executeUpdate();

			// 新規登録成功時の処理
			System.out.println("新規登録に成功しました");

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//詳細参照
	public User reference(String id) {
		Connection con = null;
		try {
			// データベースへ接続
			con = DBManager.getConnection();


			// 確認済みのSQL
			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			// 登録失敗時の処理
			if (!rs.next()) {
				return null;
			}
			// 登録成功時の処理
			int IdData = rs.getInt("id");
			String nameData = rs.getString("name");
			String mailData = rs.getString("user_name");

			System.out.println("詳細表示");

			return new User(IdData, nameData, mailData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	//ユーザーネーム,パスワード確認メソッド
		public User findByNameandPassowrd(String userName,String password) {
			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// 確認済みのSQL
				String sql = "SELECT * FROM user WHERE user_name = ? and password = ?";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, userName);
				pStmt.setString(2, password);
				ResultSet rs = pStmt.executeQuery();

				// 登録失敗時の処理
				if (!rs.next()) {
					return null;
				}

				// 登録成功時の処理
				String mailData = rs.getString("user_name");
				String passData = rs.getString("password");

				return new User(mailData,passData);

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
	//暗号化メソッド
		public String seacret(String password) {
			//ハッシュを生成したい元の文字列
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = null;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			String result = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			return result;



		}


}
