package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import Base.DBManager;
import beans.Shop;

public class ShopDAO {

		//検索機能


		public List<Shop> findbySearch(String startPrice,String endPrice,String station){
			Connection con = null;
			List<Shop> shopList = new ArrayList<Shop>();

			try {
				// データベースへ接続
				con = DBManager.getConnection();

				//SELECT文の準備
				String sql = "SELECT * FROM shop WHERE id != 0";

				if(!startPrice.equals("")) {
					sql += " AND start_price >= '" + startPrice + "'" ;
				}
				if(!endPrice.equals("")) {
					sql += " AND start_price <=  '" + endPrice + "'" ;
				}

				if(!station.equals("")) {
					sql += " AND station = '" + station + "'";
				}

//				if(!startPrice.equals("")) {
//					sql += " AND start_price >= '" + startPrice + "'" ;
//				}
//				if(!endPrice.equals("")) {
//					sql += " AND start_price <=  '" + endPrice + "'" ;
//				}
//
//				if(!station.equals("")) {
//					sql += " AND station = '" + station + "'";
//				}

				//SELECT文を実行し、結果表(ResultSet)を取得
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				// 結果表に格納されたレコードの内容を
				// Userインスタンスに設定し、ArrayListインスタンスに追加
				while (rs.next()) {
					int id = rs.getInt("id");
					String name = rs.getString("name");
					String address = rs.getString("address");
					String phoneNumber = rs.getString("phone_number");
					int startprice = rs.getInt("start_price");
					int endprice = rs.getInt("end_price");
					String seatsNumber = rs.getString("seats_number");
					String privateRoom = rs.getString("private_room");
					Time startTime = rs.getTime("start_time");
					Time endTime = rs.getTime("end_time");
					String Station = rs.getString("station");
					String fileName = rs.getString("file_name");
					int star = rs.getInt("star");
					String transportation = rs.getString("transportation");
					String fileName2 = rs.getString("file_name2");
					String fileName3 = rs.getString("file_name3");

					Shop shop = new Shop(id, name, address, phoneNumber, startprice, endprice, seatsNumber,privateRoom,startTime,endTime,Station,fileName,star,transportation,fileName2,fileName3);

					System.out.println("検索完了");

					shopList.add(shop);

				}

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return shopList;
		}


		//ランキング機能
		public List<Shop> rank(){
		Connection con = null;
		List<Shop> rankList = new ArrayList<Shop>();

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			//SELECT文の準備
			String sql = "SELECT * FROM shop ORDER BY  star DESC LIMIT 3 ";



			//SELECT文を実行し、結果表(ResultSet)を取得
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				String phoneNumber = rs.getString("phone_number");
				int startprice = rs.getInt("start_price");
				int endprice = rs.getInt("end_price");
				String seatsNumber = rs.getString("seats_number");
				String privateRoom = rs.getString("private_room");
				Time startTime = rs.getTime("start_time");
				Time endTime = rs.getTime("end_time");
				String Station = rs.getString("station");
				String fileName = rs.getString("file_name");
				int star = rs.getInt("star");
				String transportation = rs.getString("transportation");
				String fileName2 = rs.getString("file_name2");
				String fileName3 = rs.getString("file_name3");

				Shop shop = new Shop(id, name, address, phoneNumber, startprice, endprice, seatsNumber,privateRoom,startTime,endTime,Station,fileName,star,transportation,fileName2,fileName3);

				System.out.println("ランキング");
				rankList.add(shop);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return rankList;
		}

		//店舗詳細
		public Shop syousai(String id) {
			Connection con = null;
			try {
				// データベースへ接続
				con = DBManager.getConnection();


				// 確認済みのSQL
				String sql = "SELECT * FROM shop WHERE id = ?";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = con.prepareStatement(sql);
				pStmt.setString(1, id);

				ResultSet rs = pStmt.executeQuery();

				// 登録失敗時の処理
				if (!rs.next()) {
					return null;
				}
				// 成功時の処理
				int idData = rs.getInt("id");
				String nameData = rs.getString("name");
				String addressData = rs.getString("address");
				String phoneNumberData = rs.getString("phone_number");
				int startpriceData = rs.getInt("start_price");
				int endpriceData = rs.getInt("end_price");
				String seatsNumberData = rs.getString("seats_number");
				String privateRoomData = rs.getString("private_room");
				Time startTimeData = rs.getTime("start_time");
				Time endTimeData = rs.getTime("end_time");
				String StationData = rs.getString("station");
				String fileNameData = rs.getString("file_name");
				int starData = rs.getInt("star");
				String transportationData = rs.getString("transportation");
				String fileNameData2 = rs.getString("file_name2");
				String fileNameData3 = rs.getString("file_name3");


				System.out.println("店舗詳細表示");

				return new Shop(idData,nameData,addressData,phoneNumberData,startpriceData,endpriceData,seatsNumberData,privateRoomData,startTimeData,endTimeData,StationData,fileNameData,starData,transportationData,fileNameData2,fileNameData3);

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}

		}

}
