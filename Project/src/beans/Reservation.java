package beans;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;

public class Reservation implements Serializable{

	private int id;
	private int userId;
	private int shopId;
	private Date reservationDate;
	private Time reservationTime;
	private Date createDate;

	public Reservation() {

	}


	public Reservation(int id,int userId,int shopId,Date reservationDate,Time reservationTime,Date createDate) {

		this.id = id;
		this.userId = userId;
		this.shopId = shopId;
		this.reservationDate = reservationDate;
		this.reservationTime = reservationTime;
		this.createDate = createDate;
	}




	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}

	public Date getReservationDate() {
		return reservationDate;
	}
	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}
	public Time getReservationTime() {
		return reservationTime;
	}
	public void setReservationTime(Time reservationTime) {
		this.reservationTime = reservationTime;
	}

	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(reservationDate);
	}
	public String getFormatData1() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH時mm分");
		return sdf.format(reservationTime);
	}

}
