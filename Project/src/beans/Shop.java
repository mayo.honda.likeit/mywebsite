package beans;

import java.io.Serializable;
import java.sql.Time;
import java.text.SimpleDateFormat;

public class Shop implements Serializable{
	private int id;
	private String name;
	private String address;
	private String phoneNumber;
	private int startPrice;
	private int endPrice;
	private String seatsNumber;
	private String privateRoom;
	private Time startTime;
	private Time endTime;
	private String station;
	private String fileName;
	private int star;
	private String transportation;
	private String fileName2;
	private String fileName3;


	public Shop() {

	}

	public Shop(int startPrice, int endPrice,String station, String fileName,int star) {

		this.startPrice = startPrice;
		this.endPrice = endPrice;
		this.station = station;
		this.fileName = fileName;
		this.star = star;

	}




	public Shop(int id, String name, String address, String phoneNumber, int startPrice, int endPrice,
			String seatsNumber, String privateRoom, Time startTime, Time endTime, String station, String fileName,int star,String transportation,String fileName2,String fileName3) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.startPrice = startPrice;
		this.endPrice = endPrice;
		this.seatsNumber = seatsNumber;
		this.privateRoom = privateRoom;
		this.startTime = startTime;
		this.endTime = endTime;
		this.station = station;
		this.fileName = fileName;
		this.star = star;
		this.transportation = transportation;
		this.fileName2 = fileName2;
		this.fileName3 = fileName3;
	}




	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public int getStartPrice() {
		return startPrice;
	}
	public void setStartPrice(int startPrice) {
		this.startPrice = startPrice;
	}
	public int getEndPrice() {
		return endPrice;
	}
	public void setEndPrice(int endPrice) {
		this.endPrice = endPrice;
	}
	public String getSeatsNumber() {
		return seatsNumber;
	}
	public void setSeatsNumber(String seatsNumber) {
		this.seatsNumber = seatsNumber;
	}
	public String getPrivateRoom() {
		return privateRoom;
	}
	public void setPrivateRoom(String privateRoom) {
		this.privateRoom = privateRoom;
	}
	public Time getStartTime() {
		return startTime;
	}
	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}
	public Time getEndTime() {
		return endTime;
	}
	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}
	public String getStation() {
		return station;
	}
	public void setStation(String station) {
		this.station = station;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getStar() {
		return star;
	}
	public void setStar(int star) {
		this.star = star;
	}
	public String getTransportation() {
		return transportation;
	}

	public void setTransportation(String transportation) {
		this.transportation = transportation;
	}

	public String getFormatData() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH時mm分");
		return sdf.format(startTime);
	}

	public String getFormatData1() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH時mm分");
		return sdf.format(endTime);
	}
	public String getFileName2() {
		return fileName2;
	}

	public void setFileName2(String fileName2) {
		this.fileName2 = fileName2;
	}

	public String getFileName3() {
		return fileName3;
	}

	public void setFileName3(String fileName3) {
		this.fileName3 = fileName3;
	}


}
