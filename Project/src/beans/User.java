package beans;

import java.io.Serializable;

public class User implements Serializable{
	private int id;
	private String name;
	private String userName;
	private String password;


	public User() {

	}

	//詳細参照
	public User(int id, String name, String userName) {
		this.id = id;
		this.name = name;
		this.userName = userName;
	}


	public User( String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	public User(int id, String name, String userName, String password) {
		this.id = id;
		this.name = name;
		this.userName = userName;
		this.password = password;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}


}
