package beans;

import java.io.Serializable;

public class Evaluation implements Serializable{
	private int id;
	private int userId;
	private int shopId;
	private int star;
	private String comment;

	public Evaluation() {

	}

	public Evaluation(int id, int userId, int shopId, int star,String comment) {

		this.id = id;
		this.userId = userId;
		this.shopId = shopId;
		this.star = star;
		this.comment = comment;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public int getStar() {
		return star;
	}
	public void setStar(int star) {
		this.star = star;
	}
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}


}
