

CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;

USE mywebsite;


CREATE TABLE user(
id int PRIMARY KEY AUTO_INCREMENT,
name varchar(256) NOT NULL,
user_id varchar(256) NOT NULL,
password varchar(256) NOT NULL);

CREATE TABLE reservation(
id int PRIMARY KEY AUTO_INCREMENT,
user_id int NOT NULL,
shop_id int NOT NULL,
create_date datetime NOT NULL,
start_time time NOT NULL,
end_time time NOT NULL,
reservation_date datetime NOT NULL);

CREATE TABLE evaluation(
id int PRIMARY KEY AUTO_INCREMENT,
user_id int NOT NULL,
shop_id int NOT NULL,
star int NOT NULL);

CREATE TABLE shop(
id int PRIMARY KEY AUTO_INCREMENT,
name varchar(256) NOT NULL,
address varchar(256) NOT NULL,
phone_number varchar(256) NOT NULL,
start_price int NOT NULL,
end_price int NOT NULL,
seats_number varchar(256) NOT NULL,
private_room varchar(256) NOT NULL,
start_time time NOT NULL,
end_time time NOT NULL,
station varchar(256) NOT NULL,
file_name varchar(256) NOT NULL);


--データ登録--

INSERT INTO user VALUES(1,'本田','honda@gmail.com','12345678');

--店舗--
INSERT INTO shop VALUES
(1,'YORONIKU','東京都渋谷区恵比寿1-11-5 GEMS恵比寿 8F','03-3440-4629',13000,18000,'58席','有','17:00','24:00','恵比寿駅','YORONIKU.jpg'),
(2,'焼肉 ジャンボ はなれ','東京都文京区本郷3-27-9 アンリツビル B1F〜1F','03-5689-8705',8000,13000,'25席','有','17:00','24:00','本郷三丁目駅','HANARE.jpg'),
(3,'スタミナ苑','東京都足立区鹿浜3-13-4','03-3897-0416',6000,8000,'52席','無','16:00','22:00','西新井大師西駅','SUTAMINAEN.jpg'),
(4,'炭火焼 ゆうじ ','東京都渋谷区宇田川町11-1 松沼ビル　１Ｆ','03-3464-6448',6000,10000,'40席','無','18:00','23:30','渋谷駅','YUZI.jpg'),
(5,'焼肉 静龍苑 ','東京都江東区常盤2-14-11','03-3632-2348',15000,20000,'20席','有','17:00','24:00','清澄白河駅','SEIRYUUEN.jpg'),
(6,'在市 月島本店 ','東京都中央区月島2-14-8 AMビル1,2F','050-5869-3869',4000,6000,'42席','無','17:00','23:00','月島駅','ZAITI.jpg'),
(7,'焼肉うしごろ 銀座店 ','東京都中央区銀座1-6-6','050-5890-8451',10000,17000,'74席','有','17:00','24:00','銀座駅','USIGORO.jpg'),
(8,'焼肉 鶯谷園','東京都台東区根岸1-5-15','03-3874-8717',6000,8000,'30席','無','15:00','24:00','鶯谷駅','UGUISUDANIEN.jpg'),
(9,'正泰苑','東京都荒川区町屋8-7-6','03-3895-2423',6000,10000,'40席','無','17:00','24:00','町屋駅,','SYOUTAIEN.jpg'),
(10,'焼肉しみず ','東京都品川区西五反田4-29-13 TYビル 2F','03-3492-2774',8000,12000,'42席','無','17:00','23:00','不動前駅','SIMIZU.jpg');


ALTER TABLE user CHANGE COLUMN user_id user_name varchar(256) NOT NULL; 

 DROP TABLE reservation；
